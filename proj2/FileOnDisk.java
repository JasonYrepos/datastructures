/**
 * This is a FileOnDIsk class which will obtain data from a file (the size of the file and 
 * the path it is located).
 * @author Jason Yang
 *
 */
public class FileOnDisk implements Comparable<FileOnDisk> {
	private long size;
	private String absPath;
	FileOnDisk(String absPath, long size){
		this.size = size;
		this.absPath = absPath;
	}
	/**
	 * This method takes the raw size of the file by bytes then translates it
	 * accordingly by dividing by powers of 1024 and it is set to show 2 decimal places. 
	 * @return a String with translated size appended with appropriate unit and path.
	 */
	public String toString(){
		if (size < 1024 ) 
			return String.format("%.2f",(float) size)  + " B" + "\t\t" + absPath;
		else if (size/1024 < 1024 )
			return String.format("%.2f",(float)( size / 1024.0 )) + " KB" + "\t" + absPath;
		else if (size/1024/1024 < 1024 )
			return	String.format("%.2f",(float)( size / (1024.0 * 1024))) + " MB" + "\t" + absPath;
		else 
			return	String.format("%.2f",(float)( size / (1024.0 * 1024*1024))) + " GB" + "\t" + absPath;
		
	}
	/**
	 * An accessor method for AbsPath
	 * @return returns the absolute path 
	 */
	public String getAbsPath(){
		return absPath;
		
	}
	/**
	 * An accessor method for size
	 * @return returns the file size
	 */
	public float getSize(){
		return size;
		
	}
	@Override
	/**Implemented by Comparable. It is a method to allow sorting according to size from
	 * smallest to largest. It will also use the string compareTo method in case
	 * sizes are equal.
	 * @param o the FileOnDisk object that this object is comparing with
	 * @return 0,1,-1 according to results from comparison.
	 */
	public int compareTo(FileOnDisk o) {
	
		if(size > o.size){
			return 1;
		} else if(size < o.size){
			return -1;
		} else if(size == o.size){
			return o.absPath.compareTo(this.absPath);
		//	return this.absPath.compareTo(o.absPath);
		}
		return 0;
	}
}
