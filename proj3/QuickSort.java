package proj3;

/**
 * 
 * @author Jason Yang
 *
 */
public class QuickSort<E extends Comparable<E>> implements Sorter<E> {
/**
 * Quick Sort algorithm through swapping and returns a pivot index so that the function can be 
 * called onto its subarrays to the right of the pivot index and to the left.
 * @param array - the input array to have its elements swapped
 * @param left - the leftmost element of the input array
 * @param right - the rightmost element of the input array
 * @param pivoti - the pivot which will swap with elements in the array
 * @return the next pivot index
 */
	public int partit(E[] array, int left, int right, int pivoti){
		// swaps pivot with the last element of the array
		E temp = array[right];
		array[right] = array[pivoti];
		array[pivoti] = temp;
		pivoti = right;
		right = right- 1;
		//moves the "finger" until left "finger" sees an element greater than the pivot and
		//right "finger" sees an element less than the pivot or until they cross.
		while(left <=  right){
			while (array[left].compareTo(array[pivoti]) < 0){
				left++;
			}
			while(right >= left && array[right].compareTo(array[pivoti]) >= 0){
				right --;
			}
			if (right > left){
				E tempt = array[left];
				array[left] = array[right];
				array[right] = tempt;
			}
		}
		//switch the element where the "finger" stopped with the pivot
 		E tempt2 = array[left];
		array[left] = array[pivoti];
		array[pivoti] = tempt2;
		return left;
	}
	/**
	 * recursion of the partitioning function so that it sort the array and its subarrays,eventually forming
	 *  a fully sorted array.
	 * @param array the array or subarrays to be sorted
	 * @param left the leftmost index of the array/subarray
	 * @param right the rightmost index of the array/subarray
	 */

	public void quicksortrecursion(E[] array, int left, int right){
		if (right <= left){
			return;
		}
		//Improvement: check to see if there are less than 25 elements in the subarray, then it will use
		// insertion sort on the subarrays instead of recursively sorting/dividing.
		if(right - left + 1 < 25){
			CustomInsertionSort(array,left,right + 1);
		} else{
		int pivind = (left + right)/2;
		int newpiv = partit(array,left,right,pivind);
			quicksortrecursion(array,left, newpiv - 1);
			quicksortrecursion(array,newpiv + 1, right);
		}
		}	
	

	@Override
	/** sorts the array using quicksort
	 * @list - array to be sorted
	 */
	public void sort(E[] list) {
		// TODO Auto-generated method stub
		quicksortrecursion(list,0, list.length - 1);
	
		
		
}
	/**
	 * Sorts part of the list given the start value and finish value with insertion sort.
	 * @param list complete array which will be sorted according to value of start and finish
	 * @param start - the beginning index which will start the sorting
	 * @param finish - the last index when the sorting ends
	 */
	public void CustomInsertionSort(E[] list, int start, int finish){
		for(int i = start + 1; i < finish; i++){
			E currentElement = list[i];
			int k;
			for(k = i - 1; k >= 0 && list[k].compareTo(currentElement) > 0; k--){
				list[k + 1] = list[k];
			}
			list[k + 1] = currentElement;
		}
	}

	}