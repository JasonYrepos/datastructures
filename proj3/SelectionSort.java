package proj3;
/**
 * 
 * @author Jason Yang
 *
 */
public class SelectionSort<E extends Comparable<E>> implements Sorter<E> {
/** sorts the list using selection sort by switching the smallest elements.
 * @param list list to be sorted
 */
	@Override
	public void sort(E[] list) {

		E temp;
		int mindex;
		for(int i =0; i< list.length ; i++){
			
			mindex = i;
			for(int j = i + 1; j < list.length; j++){
				if(list[mindex].compareTo(list[j]) > 0){
					mindex = j;
				}
				
			}
			temp = list[i];
			list[i] = list[mindex];
			list[mindex] = temp;
		}
		
	}

	}
