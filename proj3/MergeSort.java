package proj3;

import java.lang.reflect.Array;
/**
 * 
 * @author Jason Yang
 *
 * 
 */
public class MergeSort<E extends Comparable<E>> implements Sorter<E> {
	/** Sorts the array using merge sort
	 * @list list- array to be sorted
	 */
	@Override
	public void sort(E[] list) {
		
		int length = list.length;
		@SuppressWarnings("unchecked")
		E[] DividedArray = (E[]) Array.newInstance(list.getClass().getComponentType(),length);   
	    mergesortrecursion(list,DividedArray,0, length - 1);
	    
	}
		/**
		 * It recursively calls itself, splitting the array into two parts continuously until 
		 * it can't split anymore, then it calls the merge function to merge and at the same time
		 * sort the resulting arrays.
		 * @param list
		 * @param DividedArray
		 * @param left
		 * @param right
		 */
	   public void mergesortrecursion(E[] list, E[] DividedArray, int left, int right) {

		   	  if (left - right >= 0){ 
		   		  return;
	    	 //base case: when the list can't be divided any furthur
	    	  }else{
	    	  int mid = (left+right)/2;         
	    	  mergesortrecursion(list, DividedArray, left, mid);     
	    	  mergesortrecursion(list, DividedArray, mid+1, right);  
	    	  merge(list,DividedArray,left,right,mid);
	    	  }
	    	}
	   /**
	    * It looks at the arrays left of and right of the midpoint and it
	    * merges them back into the parent array.
	    * @param list main array to be sorted
	    * @param DividedArray - temporary array which will be split up recursively from mergesortrecursion
	    * @param left - leftmost index of the array
	    * @param right - rightmost index of the array 
	    * @param mid - middle index of the array
	    */
	   @SuppressWarnings({ "rawtypes", "unchecked" })
	public void merge(E[] list, E[] DividedArray, int left, int right,int mid ){
		   //Improvement: when the array has a certain size, it doesn't have to be broken apart and 
		   //merged further since it will sorted by insertion sort.
		   InsertionSort InsertSort = new InsertionSort();
		   if(list.length <= 25){ 	    		
	    		InsertSort.sort(list);
	    	}else{
		   	  for (int i=left; i<=right; i++){    
		    	    DividedArray[i] = list[i];
		   	  }
		    	  int indexleft = left;
		    	  int highmid = mid + 1;
		    	  int indexright = highmid;
		    	  int index = left;
		   
		   while(index <= right){
		    	  
		    	  if (!(indexleft < highmid)){             
		    		  	list[index] = DividedArray[indexright];
		    		  	indexright++;
		    	  }
		    	  	else if (!(indexright <= right)){             
		    	  		list[index] = DividedArray[indexleft];
		    	  		indexleft++;
		    	  }
		    	  	else {
		    	  		if (DividedArray[indexleft].compareTo(DividedArray[indexright]) <= 0){
		    	  			list[index] = DividedArray[indexleft];
		    	  			indexleft++;
		    	  			}
		    	  		else{
		    	  			list[index] = DividedArray[indexright];
		    	  			indexright++;
		    	  		}
		    	  	}
		    	  index++;
		    	  }
		    	
//// less efficient method :
//		    	  while(indexleft< highmid && indexright <= right){
//			    		 
//		    		  if (DividedArray[indexleft].compareTo(DividedArray[indexright]) <= 0){ 
//		    			  list[index] = DividedArray[indexleft];
//		    			  indexleft++;
//		    			  }
//			    	    else
//			    	    	{ list[index] = DividedArray[indexright];
//			    	    	  indexright++;
//			    	    	}
//		    		  	index++;
//		    	  		}	
//		    	 
//			      while(index <= right){
//			    	   if (indexleft>= mid+1 && indexright < right)  {    
//			    		   list[index] = DividedArray[indexright];
//			    		   indexright++;
//			    	   }
//				    	    else if (indexright > right && indexleft< highmid)            
//				    	    {  list[index] = DividedArray[indexleft];
//				    	    indexleft++;
//				    	    }
//			    	  index++;
//			      }
	    	}
	   }

}
