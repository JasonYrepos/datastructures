package proj4;
/**
 * 4/14/15
 * @author Jason Yang
 *	Nodes to be used in creating linkedlists. It holds a generic data variable 
 * and has a next reference to another node.
 * @param <E> Generic class
 */
public class node<E> {
	private E data = null;
	private node<E> next = null;
	/**
	 * Constructor for a empty node
	 */
	public node(){
		
	}
	/**
	 * Constructor for a node that takes two parameters.
	 * @param data - sets the data of the node
	 * @param next - sets the reference to the next node
	 */
	public node(E data, node<E> next){
		this.data = data;
		this.next = next;
	}
	/**
	 * getter method to obtain the next node
	 * @return next node
	 */
	public node<E> getnext(){
		return next;
	}
	/**
	 * getter method to obtain data of this node
	 * @return data of this node
	 */
	public E getdata(){
		return data;
	}
	/**
	 * setter method to set next to reference the input next node.
	 * @param inputnext the next node
	 */
	public void setnext(node<E> inputnext ){
		this.next = inputnext;
	}
	/**
	 * setter method to set data to inputdata.
	 * @param inputdata the data to be set for this node
	 */
	public void setdata(E inputdata){
		this.data = inputdata;
	}
}