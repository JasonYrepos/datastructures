package proj4;

import proj4.node;
/**
 * 4/14/15
 * @author Jason Yang
 * Linkedlist implementation of a Stack
 * @param <E> Generic class
 */
public class MyStack<E> {
	private node<E> head = null;
	/**
	 * Constructor which forms an empty Stack.
	 */
	public MyStack(){
		
	}
	/**
	 * adds a new node containing data of item to the head of a linkedlist 
	 * or if there isn't a head, the head is created with data of item.
	 * @param item
	 * @return
	 */
	public E push(E item){
		if(head == null){
			head = new node<E>(item,null);
		}else{
			head = new node<E>(item,head);
		
		}
		return item;
		
		
	}
	/**
	 * returns the data that the head node has then the head points to the next
	 * node in the linkedlist.
	 * @return the head that was popped out of the list or the original head.
	 * @throws NullPointerException when Stack or linkedlist is empty
	 */
	public E pop() throws NullPointerException{
		E holder = null;
		try{
			holder = head.getdata();
			head = head.getnext();
		}catch(Exception e){
			System.err.println("Cannot pop a Empty Stack");
			System.exit(0);
		
		}
		return holder;
	}

	/**
	 * Return data from the stack without affecting node on the stack
	 * @return data value from the head of the linkedlist 
	 * @throws NullPointerException when Stack or linkedlist is empty
	 */

	public E peek() throws NullPointerException{
		E holder = null;
		try{
			holder = head.getdata();
		}catch(Exception e){
			System.err.println("Cannot peek at a Empty Stack");
			System.exit(0);
		}
		return holder;
	}
	/**
	 * Checks if the head is null meaning that the linkedlist is empty.
	 * @return whether the linkedlist is empty
	 */
	public boolean empty(){
		if(head == null){
			return true;
		}
		return false;
	}
}
