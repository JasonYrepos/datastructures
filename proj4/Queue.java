package proj4;

import proj4.node;
/**
 * 4/14/15
 * @author Jason Yang
 *	Linkedlist implementation of a Queue.
 * @param <E> Generic class
 */
public class Queue<E> {
	private node<E> head = null;
	/**
	 * Constructor method which forms an empty queue;
	 */
	public Queue(){
	
	}
	/**
	 * adds a node with data of item to the end of a linked list. If 
	 * there is no linkedlist/head isn't set then the head will be set to a new node
	 * with item data.
	 * @param item data to be placed in a new node which will be added to the linkedlist
	 * @return the data item
	 */
	public E enqueue(E item){
		node<E> current = null;
		if(head != null){
		current = head;
		}else{
			head = new node<E>(item,null);
			return item;
		}
		while(current.getnext() != null){
			current = current.getnext();
		}
		current.setnext(new node<E>(item,null));
		return item;
	}
	/**
	 * a temporary variable will hold the data of the head of the linkedlist if it isn't
	 * null. Then the head would become the next node. 
	 * @return the data from the head node if there is one
	 * @throws NullPointerException when Queue or linkedlist is empty
	 */
	public E dequeue() throws NullPointerException{
		E holder = null;
		try{
		holder = head.getdata();
		head = head.getnext();
		}catch(Exception e){
			System.err.println("Cannot dequeue a Empty queue");
			System.exit(0);
		}
		
		
		return holder;
	}
	/**
	 * Loops through the linkedlist till it finds the last node. 
	 * @return the last node of the linkedlist without changing the nodes of the linkedlist.
	 * @throws NullPointerException when Queue or linkedlist is empty
	 */
	public E peek() throws NullPointerException{
		node<E> current = null;
		try{
		current = head;
		while(current.getnext() != null){
			current = current.getnext();
		}
		} catch(Exception e){
			System.err.println("Cannot peek a Empty queue");	
			System.exit(0);
		}
		return current.getdata();
	}
	/**
	 * Checks if the head is null meaning that the linkedlist or this Queue is empty.
	 * @return whether the linkedlist is empty
	 */
	public boolean empty(){
		if(head == null){
			return true;
		}
		return false;
	}

}
