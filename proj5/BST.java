package proj5;



import java.io.PrintWriter;

/**
 * Provides recursive implementation of a binary search tree (BST). 
 * @author Jason Yang and credits to Joanna Klukowska for the recursive insert,remove recursive BST methods.
 *
 */
public class BST  {
	private PrintWriter writer ;
	private BSTNode root;
	private int cuttoff;
	private int numOfElements;
	/**
	 * getter method for number of nodes in the tree
	 * @return number of nodes in tree or unique words
	 */
	public int getnumOfElements(){
		return numOfElements;
	}
	/**
	 * setter method for cut off by count of words to be pruned
	 * @param cuttofflength cut off length for words
	 */
	public void setcuttoff(int cuttofflength){
		this.cuttoff = cuttofflength;
	}
	
	/**
	 * Creates an empty BST.
	 */
	public BST() {
		this.root = null;
		numOfElements = 0;
	}

	/**
	 * Add an item to this BST.
	 * @param item a new element to be added to the tree (ignored if null)
	 */
	public void insert(String item) {
		if (item!=null)
			root = recInsert(new Info(item), root);

	}

	/*
	 * Recursively add an item to this BST alphabetically. If a unique word is alphabetically ahead 
	 * or in front of a node, it will be placed to the left and if it is alphabetically later it will be placed
	 * to to the right. If it isn't a unique word, it will be found in the tree and the count of 
	 * the word will increase. If added the number of elements will increment accordingly.
	 * @param item item to be added
	 * @param tree root of the subtree into which the node will be added
	 * @return reference to this BST after the item was inserted
	 */
	private BSTNode recInsert(Info data, BSTNode tree) {
		if (tree == null) {
			// Addition place found
			tree = new BSTNode(data.getItem());
			numOfElements++;
		} else if (data.getItem().compareTo(tree.getData().getItem()) < 0){
			if(data.getItem().compareTo(tree.getData().getItem()) == 0){
				tree.getData().setCount(tree.getData().getCount() + 1);
			}else{
				tree.setLeft(recInsert(data, tree.getLeft())); // Add in left
			}
		}												
		else{
			if(data.getItem().compareTo(tree.getData().getItem()) == 0){
				tree.getData().setCount(tree.getData().getCount() + 1);
			}else{
				tree.setRight(recInsert(data, tree.getRight())); // Add in right
			
			}
			
		}													
		return tree;
	}


	/*
	 * Recursively remove an item from this BST.
	 * @param item  item to be removed
	 * @param tree  root of the subtree from which the item will be removed 
	 * @return reference to this BST after the item was removed 
	 */
	private BSTNode recRemove(String data, BSTNode tree) {

		if (tree == null)
			; // do nothing, item not found
		else if (data.compareTo(tree.getData().getItem()) < 0){
			tree.setLeft(recRemove(data, tree.getLeft()));
		}
		else if (data.compareTo(tree.getData().getItem()) > 0){
			tree.setRight(recRemove(data, tree.getRight()));
		}
		else {
			tree = removeNode(tree);
		}
		return tree;
	}

	/*
	 * Remove a particular node - the actual action depends on number of 
	 * children that the node has. Decrements the number of elements accordingly.
	 * @param tree the node to be removed 
	 * @return reference to this BST after node was removed 
	 */
	private BSTNode removeNode(BSTNode tree) {
		Info data;


		if (tree.getLeft() == null) {
			if(tree.getRight() != null){
			}
			numOfElements--;
			return tree.getRight();
		}
		else if (tree.getRight() == null) {
			numOfElements--;
			return tree.getLeft();
		}
		else {
			data = getPredecessor(tree.getLeft());
			tree.setData(data);
			tree.setLeft(recRemove(data.getItem(), tree.getLeft()));
			return tree;
		}
	}

	/*
	 * Obtains the predecessor of a node (according to BST ordering). 
	 * @param tree node whose predecessor we are after
	 * @return the data contained in the predecessor node
	 */
	private Info getPredecessor(BSTNode tree) {
		while (tree.getRight() != null){
			tree = tree.getRight();
		}
		return tree.getData();
	}
	/**
	 * Using postorder transversal, it will check each nodes to see if the count of the word
	 * is lower than the cut off then it will delete the node according to its children.
	 * @param tree - tree or subtree to be pruned
	 * @return reference to a BSTNode which will depend accordingly to number of children of the tree or subtree
	 */
	private BSTNode pruning(BSTNode tree){
		
		if(tree != null){
			tree.setLeft(pruning(tree.getLeft()));
			tree.setRight(pruning(tree.getRight()));
			 if(tree.getData().getCount() < cuttoff){
				 return removeNode(tree);	
			 }

			}
		return tree;
		}
	
	/**
	 * prunes the entire tree
	 */
	public void prune(){
		root = pruning(root);

	}	
	/**
	 * sets the printwriter to write to the new file
	 * @param writer - printwriter that will write to file
	 */
	public void setwriter(PrintWriter writer){
		this.writer = writer;
	}
	/**
	 * uses recursive inorder transversal to write the count and their words to the file 
	 * @param tree the tree to be transversed
	 */
	public void writetofile(BSTNode tree){
		
		if(tree != null){
			writetofile(tree.getLeft());
			writer.println(tree.getData().getCount() +" "+  tree.getData().getItem() );
			writetofile(tree.getRight());
			
		}
	}
	/**
	 * writes to file the entire tree in alphabetic order
	 */
	public void writefile(){
		writetofile(root);
	}
	
	


}
