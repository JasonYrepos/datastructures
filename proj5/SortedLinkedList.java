package proj5;
/**
 * Sorted linkedlist that will add elements alphabetically and remove node according to the cuttoff.
 * @author Jason Yang
 *
 */
public class SortedLinkedList {
	private Node head = null;
	private int cuttoff;
	private int numberwords = 0;
	/**
	 * A empty linkedlist constructor
	 */
	public SortedLinkedList(){
		
	}
	/**
	 * Constructor of a linkedlist with a head that holds a node containing a word.
	 * @param item - word to be placed in head node
	 */
	public SortedLinkedList(String item){
		this.head = new Node(item,null);
	}
	/**
	 * getter method for number of node in list
	 * @return number of nodes in linkedlist or non repeated words
	 */
	public int getnumberwords(){
		return numberwords;
	}
	/**
	 * setter method for cut off after pruning or deleting
	 * @param cuttofflength cutt off length or count of nodes 
	 */
	public void setcuttoff(int cuttofflength){
		this.cuttoff = cuttofflength;
	}
	/**
	 * delete method that will prune the linkedlist for words that have lower than the cuttoff.
	 * Checks if count of a node is under a cutt off and if it is it will be deleted. It uses
	 * a follow node that will be behind the current node which is checking the count. When
	 * deleting the follow node will point to next of the current node so that the current
	 * node is deleted.
	 */
	public void delete(){
		if(head == null){
			return;
		}
		Node current = head.getNext();
		Node follow = head; 
		while(current != null){
			if(current.getData().getCount() < cuttoff){
				follow.setNext(current.getNext());
				numberwords--;
			}else{
		follow = current;
		}
		current = current.getNext();	
		}
		//checks the head if it is under cuttoff since current starts at next of head
		if(head.getData().getCount() < cuttoff){
			head = head.getNext();
			numberwords--;
		}
	}
/**
 * adds nodes to linked list alphabetically. It first checks if the input item is 
 * going to be alphabetically first which will make the input the new head and if 
 * it is equal the count of head will increase. If it should be placed after the head
 * it iterates through the linkedlist checking if it will be inserted between nodes by using 
 * a follow node or if the current node's count will increase. If the iteration reaches
 * the end, a node to the end will be added. 
 * @param item - word to be added or checked if count will increase
 */
	public void add(String item){
		if(item == null){
			return;
		}
		if(head == null){
			head = new Node(item,null);
			numberwords ++;
			return;
		}
	
		if ((head.getData().getItem().compareTo(item) > 0)){
			head = new Node(item,head);
			numberwords++;
			return;
		} else if ((head.getData().getItem().equals(item))){
			head.getData().setCount(head.getData().getCount() + 1);
			return;
		} else if(head.getData().getItem().compareTo(item) < 0){
			Node current = head.getNext();
			Node follow = head; 
			while(current != null){
	
				if(current.getData().getItem().compareTo(item) > 0){
					numberwords ++;
					Node newnode = new Node(item,current);
					follow.setNext(newnode);
				return;

					}
				if(current.getData().getItem().equals(item)){
					current.getData().setCount(current.getData().getCount() + 1);
			
					return;
					}
				if(current.getData().getItem().compareTo(item) <0){
					follow = current;
					current = current.getNext()	;	
			
				}
			}
			if(current == null){
				follow.setNext(new Node(item,null));
				numberwords++;
		}
		}

	}

}