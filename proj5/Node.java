package proj5;
/**
 * @author Jason Yang
 *	Nodes to be used in creating linkedlists. It holds a Info data variable which holds the word and count. 
 * and has a next reference to another node.
 */
public class Node {

	private Info data = null; 
	private Node next = null;
	/**
	 * Constructor for a empty node
	 */
	public Node(){
		
	}

	/**
	 * Constructor for a node that takes two parameters.
	 * @param inputdata - sets the data of the node by adding a Info object initialized with the String inputdata
	 * @param next- sets the reference to the next node
	 */
	public Node(String inputdata, Node next){
		this.setData(new Info(inputdata));
		this.setNext(next);
	}

	/**
	 * getter method to obtain data of this node
	 * @return data of this node
	 */
	public Info getData() {
		return data;
	}

	/**
	 * setter method to set data to inputdata.
	 * @param inputdata the data to be set for this node
	 */
	public void setData(Info data) {
		this.data = data;
	}
	/**
	 * getter method to obtain the next node
	 * @return next node
	 */
	public Node getNext() {
		return next;
	}

	/**
	 * setter method to set next to reference the input next node.
	 * @param inputnext the next node
	 */
	public void setNext(Node next) {
		this.next = next;
	}





	
}