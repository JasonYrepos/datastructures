package proj5;
/**
 * data field for BSTNode and Node which holds the item or word with their respective counts.
 * @author Jason Yang
 *
 */
public class Info{
	private String item = null;
	private int count = 0;
	/**
	 * Constructor with empty fields
	 */
	public Info(){
		
	}
	/**
	 * Constructor for a new word with initial count of 1
	 * @param item
	 */
	public Info(String item){
		this.setItem(item);
		setCount(1);
	}
	/**
	 * getter method for the item or word
	 * @return the word
	 */
	public String getItem() {
		return item;
	}

	/**
	 * setter method for the item or word
	 * @param item that will be set 
	 */
	public void setItem(String item) {
		this.item = item;
	}
	
	/**getter method for the count of a word
	 * @return the count
	 */
	public int getCount() {
		return count;
	}
	
	/** setter method for the count 
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}
}
