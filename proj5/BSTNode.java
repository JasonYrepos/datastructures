
package proj5;

/**
 * BSTNode class is used to represent nodes in a binary search tree.
 * It contains a Info item which holds the count and word
 * and references to left and right subtrees. 
 * 
 * @author Jason Yang credits to Joanna Klukowska
 *
 */
public class BSTNode{
	//reference to the left subtree 
	private BSTNode left;
	//reference to the right subtree
	private BSTNode right;
	//data item stored in the node
	private Info data;
	
	/**
	 * Constructs a BSTNode initializing the data part 
	 * according to the parameter and setting both 
	 * references to subtrees to null.
	 * @param data
	 *    data to be stored in the node
	 */
	public BSTNode(String data) {
		this.data = new Info(data);
		left = null;
		right = null;
	}
	
	/**
	 * Constructs a BSTNode initializing the data part
	 * and the subtree references according to the parameters.
	 * @param data
	 *    data to be stored in the node
	 * @param left
	 *    reference to the left subtree
	 * @param right
	 *    reference to the right subtree
	 */
	public BSTNode( String data, BSTNode left, BSTNode right) {
		this.left = left;
		this.right = right;
		this.data.setItem(data);
	}

	/**
	 * Left subtree accessor. 
	 * @return 
	 *    reference to the left subtree of a node
	 */
	public BSTNode getLeft() {
		return left;
	}
	
	/**
	 * Changes the reference to the left subtree to the one 
	 * specified in the parameter.
	 * @param 
	 *    reference to the new left subtree of the node.
	 */
	public void setLeft(BSTNode left) {
		this.left = left;
	}
	
	/**
	 * Right subtree accessor. 
	 * @return 
	 *    reference to the right subtree of a node
	 */
	public BSTNode getRight() {
		return right;
	}
	
	/**
	 * Changes the reference to the right subtree to the one 
	 * specified in the parameter.
	 * @param 
	 *    reference to the new right subtree of the node.
	 */
	public void setRight(BSTNode right) {
		this.right = right;
	}
	
	/**
	 * Returns a reference to the data stored in the node. 
	 * @return 
	 *    reference to the data stored in the node
	 */
	public Info getData() {
		return data;
	}
	/**
	 * Changes the data stored in the node to the one 
	 * specified in the parameter.
	 * @param 
	 *    reference to the new data of the node
	 */
	public void setData(Info data) {
		this.data = data;
	}


	
	
}
