package proj5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
/**
 * main method which will take 3 commandline arguments, check to see if they are valid. Parse 
 * the input file, and input the data parsed into a binary search tree and a linkedlist, alphabetically.
 * Then it will prune each according to the cuttoff given. Then the remaining words be a written to
 * a new file.
 * @author Jason Yang
 *
 */
public class MostFrequentWords {

	public static void main(String[] args) throws NumberFormatException, IOException {
		long start;	//timer variables to check start and end of methods
		long end;	
		//checks for 3 commandline arguments
		if(args.length != 3){
			System.err.print("3 arguments required");
			System.exit(0);
		}
	
		SortedLinkedList llist = new SortedLinkedList();
		BST BSTree = new BST();
		
		String Inputfile = args[0];
		int cuttofflength = 0;
		//checks if 2nd argument is a postive number
		try{
			cuttofflength = Integer.valueOf(args[1]);
		} catch(NumberFormatException e){
			System.err.print("Second argument is not a number");
			System.exit(0);
		}
		if(cuttofflength < 0){
			System.err.print("Second argument is not a positive number");
			System.exit(0);
		}
		
		File Outputfile = new File(args[2]);		
		
		PrintWriter writer = null;
		FileParser fparser = null;
		//checks if 1st and 3rd arguments are file that can be found or created.
		try {
			writer = new PrintWriter(Outputfile);
			fparser = new FileParser(Inputfile);
		} catch (FileNotFoundException e) {
			
			System.err.print("File Not found");
			System.exit(0);

		} 
		
		llist.setcuttoff(cuttofflength );
		BSTree.setcuttoff(cuttofflength );
		
		start = System.nanoTime();
		ArrayList<String> allwords = fparser.getAllWords();
		end = System.nanoTime();
	
		System.out.println("INFO: Reading file took " + ((end-start)/1000000) + " ms (~ " +((end - start)/1000000000f) + " seconds).");
		System.out.println("INFO: " + allwords.size() + " words read.");
		
		System.out.println();
		System.out.println("Processing using Sorted LinkedList");
		start = System.nanoTime();
		for(String i: allwords){
			llist.add(i);	//populate linkedlist alphabetically
		}
		end = System.nanoTime();
		System.out.println("INFO: Creating index took " + ((end-start)/1000000) + " ms (~ " +((end - start)/1000000000f) + " seconds).");
		System.out.println("INFO: " + llist.getnumberwords() + " words stored in index.");
		
		start = System.nanoTime();
		llist.delete();		//prune linkedlist by cuttoff
		end = System.nanoTime();
		System.out.println("INFO: Pruning index took " + ((end-start)/1000000) + " ms (~ " +((end - start)/1000000000f) + " seconds).");
		System.out.println("INFO: " + llist.getnumberwords() + " words remaining after pruning.");
		
		System.out.println();
		System.out.println("Processing using Sorted Recursive BST");
		start = System.nanoTime();
		for(String i: allwords){
		BSTree.insert(i);	//populate binary search tree alphabetically
		}
		end = System.nanoTime();
		System.out.println("INFO: "+ "Creating index took " + ((end-start)/1000000) + " ms (~ " +((end - start)/1000000000f) + " seconds).");
		System.out.println("INFO: " + BSTree.getnumOfElements() + " words stored in index.");


		start = System.nanoTime();
		BSTree.prune();		//prune the binary search tree recursively and by cuttoff
		end = System.nanoTime();
		System.out.println("INFO: Pruning index took " + ((end-start)/1000000) + " ms (~ " +((end - start)/1000000000f) + " seconds).");
		System.out.println("INFO: "+ BSTree.getnumOfElements() + " words remaining after pruning.");


		BSTree.setwriter(writer);
		BSTree.writefile();
		writer.close();

	}

}
