
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
/**
 * 
 * @author Jason Yang
 * @version Feb, 9 2015
 * This class will read the inputfile and pass a printwriter class to a TreeList object. It will also
 * filter out lines that aren't complete with the tree's information. It will write the results after calling
 * the TreeList object's method. 
 */

public class TreeInfo {
	
	/**
	 *  The command line is first checked to see if it is empty. If it isn't the first
     *  argument is taken. Then the string is checked as a file to see if it is accessible
	 *  and if it exists. Then the string, outfilename, takes the input file name and finds the
	 *  dot extension and replaces it with a dot out extension. This is then set as the name
	 *	of the file that will be created.
	 *	An exception will be thrown if any of the files aren't found.
	 * @param args - the pathname of the input file.
	 * 
	 */
	static TreeList tlist = new TreeList();

	public static void main(String[] args) throws FileNotFoundException {

		if(args.length == 0){
			System.err.println("There are no arguments");
			System.exit(0);
		}
		String filename = args[0];
		PrintWriter write = null;
		File Inputfile = new File( filename);
		if (Inputfile.exists() != true){
			System.err.print("File does not exist");
			System.exit(0);
		}
		if (Inputfile.canRead() != true){
			System.err.print("File cannot be read");
			System.exit(0);
		}
		String outfilename = filename.substring(0, filename.lastIndexOf(".")).concat(".out");
		File Outputfile = new File(outfilename);
		String line = null;
		Scanner InputScan = null;
		Scanner species = null;
		boolean badtree = false;
		try {
			write = new PrintWriter(Outputfile);
			InputScan = new Scanner(Inputfile);
			species = new Scanner(new File("species_list.txt"));
		} catch (FileNotFoundException e) {
		
			System.err.print("File Not Found");
			e.printStackTrace();
		}

		readandaddtrees(InputScan, badtree, line, tlist);
	
	tlist.GetWriter(write);
	tlist.GetSpecies(species);
	tlist.alltask();
	write.close();
	
	}

	/**
	 * This is used to read each line of the file character by character
	 * which will be given to an array of strings. Each string in the array will append
	 * the character until it finds a comma which will change the index of the array
	 * to the next string but if it finds a quote before a comma
	 * it will not respond to a comma and instead continue adding the character to the
	 * string until it reaches a second quote or the end quote. This will prevent commas
	 * that aren't used to separate descriptors from triggering a false positive invalid
	 * tree.
	 * @param fileline - each file line of the input file
	 * @return words is an array of strings that holds the properties of the tree 
	 */
	public static String[] CustomLineParse(String fileline){
		String[] words = new String[9];
		for(int i = 0; i < words.length; i++){
		words[i] = "";
		}
			int count1 = 0;
				Boolean quotes = false;
			for(int i = 0; i < fileline.length(); i++){
				if(fileline.charAt(i) == '\"' && quotes == false){
				quotes = true;
				}else if( fileline.charAt(i) == '\"' && quotes == true){
				quotes = false;
				} else if(fileline.charAt(i) == ',' && quotes == false){
					count1++;
				}else {
					words[count1] = words[count1] + (String.valueOf(fileline.charAt(i)));
				}
				
			}
			return words;
			
	}
	/**
	 * This will read the file line by line and add trees objects to the 
	 * treelist object's arraylist accordingly if the line has
	 * 9 values to be added and if each value is valid. Note: It skips the first line which
	 * holds the title of each column in the file.
	 * @param InputScan1 Scanner that will read the input file's name in commandline argument
	 * @param badtree1 tells the program whether the line can be used to make a tree or if it
	 * doesn't meet standards such as empty information.
	 * @param line1 - each line of the input file
	 * @param tlist2 - the static Treelist will be passed so that it can add the trees from
	 * 					each line correctly. 
	 */

	public static void readandaddtrees(Scanner InputScan1, Boolean badtree1, String line1, TreeList tlist2){
	if(InputScan1.hasNextLine()){
		InputScan1.nextLine();
	}
	while (InputScan1.hasNextLine() ){
		badtree1 = false;
		line1 = InputScan1.nextLine();
		

		if(line1.split(",").length == 9){
		
			String[] lineChar = new String[9];				
			lineChar = CustomLineParse(line1);
		 	for(int i = 0; i < 9; i ++){
		 		if(lineChar[i] == ""){
		 			badtree1 = true;
			 					}
		 								}
		 	if(badtree1 == false){
		 		tlist.addtree(new Tree(lineChar[0],lineChar[1],lineChar[2],lineChar[3],lineChar[4],lineChar[5],lineChar[6],lineChar[7],lineChar[8]));
					
		 		}   
										} 
	}
	
	}
}
	
	


