/**
 * This class holds 9 strings which will be the variables of the tree object. It also
 * has accessor methods for each of these variables.	
 * @author Jason Yang
 * @version Feb 9, 2015
 */

public class Tree {

	private String treeid;
	private String street;
	private String crossStreet1;
	private String crossStreet2;
	private String treeCondition;
	private String diameter;
	private String SpecialCode;
	private String borough;
	private String zipcode;

	public Tree(String treeid1, String street1, String crosstreet11, String crosstreet21, String condition1,
			String diametermeter1,String specialcode1,String borough1,String zipcode1){
		treeid = treeid1;
		street = street1;
		crossStreet1 = crosstreet11;
		crossStreet2 = crosstreet21 ;
		treeCondition = condition1 ;
		diameter = diametermeter1;
		SpecialCode = specialcode1;
		borough = borough1 ;
		zipcode = zipcode1 ;
	}
	/** 
	 * @return - accessor method for treeid
	 */
	public String gettreeid(){
		return treeid;
	}
	/**  
	 * @return - accessor method for street
	 */
	public String getstreet(){
		return street;
	}
	/**
	 * @return - accessor method for crossStreet1
	 */
	public String getcrossStreet1(){
		return crossStreet1;
	}
	/**
	 * @return - accessor method for crossStreet2
	 */
	public String getcrossStreet2(){
		return crossStreet2;
	}
	/**
	 * @return - accessor method for treeCondition
	 */
	public String gettreeCondition(){
		return treeCondition;
	}
	/**
	 * @return - accessor method for diameter
	 */
	public String getdiameter(){
		return diameter;
	}
	/**
	 * @return - accessor method for SpecialCode
	 */
	public String getSpecialCode(){
		return SpecialCode;
	}
	/**
	 * @return - accessor method for borough
	 */
	public String getborough(){
		return borough;
	}
	/**
	 * @return - accessor method for zipcode
	 */
	public String getzipcode(){
		return zipcode;
	}
	
	

}
