
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * 
 * @author Jason Yang
 * This class will hold an arraylist of tree object called "trees" and methods to obtain
 * information about these trees will be based off this arraylist. Representation
 * of each type of trees, and zipcodes will be played in other arraylists which will
 * be able to access trees to sort themselves according to the information need such
 * as frequency of trees in a zipcode. The class has methods that will
 * print to the output file the neccessary information based upon the sorted arraylists.
 */
public class TreeList {
	private PrintWriter writefile;
	private ArrayList<Tree> trees = new ArrayList<Tree>();
	private ArrayList<String> NoRepeatTreeCodes = new ArrayList<String>();
	private ArrayList<String> ArrangedZipcode = new ArrayList<String>();
	private ArrayList <Tree> TreeDiameterList = new ArrayList<Tree>();
	private ArrayList <String[]> treekey = new ArrayList <String[]>();
	public void alltask(){
		Task12();
		Task34();
		Task5();
	}
	/**
	 * Print lines that will organize the spacing between each Task and call
	 * the functions that will fill the arraylist and write to the file the most
	 * and least popular trees.
	 */
	public void Task12(){
		Sorting(NoRepeatTreeCodes,true);
		getMostFreq(NoRepeatTreeCodes,true);
		writefile.print("\r\n");
		getLeastFreq(NoRepeatTreeCodes,true);
		
	}
	/**
	 * Print lines that will organize the spacing between each Task and call
	 * the functions that will fill the arraylist and write to the file the most
	 * and least green trees..
	 */
	public void Task34(){
		FillZipcodeLists();
		Sorting(ArrangedZipcode,false);
		writefile.print("\r\n");
		getMostFreq(ArrangedZipcode,false);
		writefile.print("\r\n");
		getLeastFreq(ArrangedZipcode,false);
		writefile.print("\r\n");
	}
	/**
	 * Gets the zipcode from the Arraylist of trees. Then it adds the zipcodes to the
	 * ArrangedZipcode ArrayList without repeats. 
	 */
	public void FillZipcodeLists(){
		for(Tree i: trees){
			if(ArrangedZipcode.contains(i.getzipcode()) == false){
				ArrangedZipcode.add(i.getzipcode());	
			}
		}
		
		 
	}
	/**
	 * calls the function that will fill the TreeDiameterList with trees of the greatest
	 * diameter. Then it loops through the list and prints on to the file the description
	 * of the tree.
	 */
	public void Task5(){
		SortThickestTree();
		writefile.println("The largest tree:\r\n");
		for(Tree a: TreeDiameterList){
			writefile.printf("%s, %s inches in diameter\r\n", translate(a.getSpecialCode(),true), a.getdiameter());
			writefile.printf("%s (%s, %s)\r\n", a.getstreet(),a.getcrossStreet1(),a.getcrossStreet2());
			writefile.printf("%s\r\n", a.getzipcode());
			
		}
	}
/**
 * this will loop trough the trees Arraylist and place a tree object with the greatest diameter
 * into the TreeDiameterList. If it finds multiple greatest diameters it will place all of them
 * into the list but once it finds a greater one during the loop, the list will become empty
 * and add the current greater diameter.
 * This will result in TreeDiameterList containing trees with the greatest diameter.
 * exception - when the diameter of a tree object isn't a number the program will exit.
 */
	public void SortThickestTree() throws NumberFormatException{
		int max = 0;
		for(int i = 1; i< trees.size();i++){
			Tree abc = trees.get(i);
			try{
				Integer.valueOf(abc.getdiameter());			
			}catch(NumberFormatException e){
				System.err.println("the tree diameter is not a number - invalid format");
				System.err.println(e);
				System.exit(0);
			}
			if(Integer.valueOf(abc.getdiameter()) > max){
				max = Integer.valueOf(abc.getdiameter());
				TreeDiameterList.clear();
				TreeDiameterList.add(abc);
			}else if(Integer.valueOf(abc.getdiameter()) == max){
				TreeDiameterList.add(abc);
			}
		}

	}
/**this will add to treekey a array that holds two strings that are taken by finding
 * splitting whitespace. The string before the first white space denotes the species code
 * and everything after it is the tree's name. The first string(species code) will be 
 * placed in the first index of the array and the rest of the string(tree's name)
 * will be placed in the second index of the array. Treekey will act like a dictionary
 * to translate the species code.
 *  Also this will place all the tree codes
 * into the NoRepeatTreeCodes Arraylist which will be used to find the most and least 
 * popular trees.
 * @param spc - a line from the species_list.txt
 * 
 */
	public void FillTreeCodeList(String spc){
		
		NoRepeatTreeCodes.add(spc.split(" ")[0].trim());
		String[] holder = new String[2];
		holder[0] = spc.split(" ")[0].trim();
		int begin = spc.indexOf(holder[0]) + holder[0].length()+1;
		
		holder[1] = spc.substring(begin).trim();
		treekey.add(holder);	
	}
	/**
	 * This will read each line of the species_list.txt file then pass the line to
	 * the function FillTreeCodeList.
	 * @param species A scanner that will access the species_list.txt file
	 */
	public void GetSpecies(Scanner species) {
		String lin;
		while (species.hasNextLine()){
			lin = species.nextLine();
		FillTreeCodeList(lin);		
								}
		
	}
/**
 * This uses the tree arraylist to count the frequency of a property such as zipcode
 * or the species code.
 * @param name - A string is passed which is either the zipcode or the species code.
 * @param isittree - isittree tells the function whether the string passed is a 
 * species code when it is true or whether it is a zipcode when the boolean is false.
 * @return - returns the number of times the zipcode or species code appears in the 
 * 			arraylist of trees.
 */
	public int Counting(String name, Boolean isittree){
		int count = 0;
		if(isittree){
			for(Tree i: trees){
				if (i.getSpecialCode().equals(name)){
					count++;
				}
			}
			return count;
		}else{
			for(Tree i: trees){
				if (i.getzipcode().equals(name)){
					count++;
				}
			}
			return count;	
		}
		
	}
	/**
	 * It checks the treekey ArrayList of string arrays. It loops through the String array
	 * and checks the first index of the array with the species code passed in. If it finds
	 * that the first index is equal, then it will return the second index which is the
	 * full name of the tree. 
	 * This doesn't account for species code not within the textfile and if it isn't found then
	 * the name will be an empty string.
	 * @param code - the code is the species code
	 * @param isittree - tells the function  whether what is being compared 
	 * 					is a species code if it is true
	 * @return - the full name of the tree 
	 */
	public String translate(String code, Boolean isittree){
		if(isittree){
		for(String[] i:treekey){
			if(i[0].equals(code)){
				return i[1];
			}
		}
		return "";
		}else {
			return code;
			}
	}
	/**
	 * Adds a Tree object into the tree list.
	 * @param tt - the tree that will passed into the method
	 */
	public void addtree(Tree tt){
		trees.add(tt);
		
	}
	/**
	 * 
	 * @param writer this will pass a Printwriter and writefile will be become a reference
	 * to be able to use the Printwriter functions.
	 */
	public void GetWriter(PrintWriter writer){
		writefile = writer;
	}
	/**
	 *
	 * This will use bubble sort to sort each string according to their frequency by
	 * using the Counting method.
	 * @param treeOrzip - An ArrayList of strings which will be passed with either the
	 * NoRepeatTreeCodes or the ArrangedZipcode.
	 * @param isittree - Tells whether it is sorting through NoRepeatTreeCodes(isittree = true)
	 * or whether it is sorting through ArrangedZipcode(isittree = false).
	 */
	public void Sorting(ArrayList<String> treeOrzip, Boolean isittree){
		for(int i = treeOrzip.size() - 1; i> 0; i--){
			for(int j = 1; j <= i; j++){
				if(Counting(treeOrzip.get(j-1),isittree) > Counting(treeOrzip.get(j),isittree)){
					String temp = treeOrzip.get(j-1);					
					treeOrzip.set(j-1,treeOrzip.get(j));
					treeOrzip.set(j, temp);	
					}
				}
		}
		
		}
	/**This function loops through an sorted arraylist from the last index which was 
	 * already sorted to have the largest corresponding count. To find the three
	 * most frequent appearances, it uses a counter called count. To account for equal
	 * amounts, it will check If the arraylist is indeed greater than the next or i-1 index.
	 * If not then there is a tie which will be printed to the file.
	 * the NoRepeatTreeCode, the translate function will return the full name of the
	 * tree code in the list which is then printed to the file.  
	 * 
	 * @param treeOrzip either the ArrangedZipcode or NoRepeatTreeCodes will be passed in
	 * @param isittree tells the function whether it should be working according to
	 * whether it should be find the most popular tree or the most green zip codes.
	 */
	public void getMostFreq(ArrayList<String> treeOrzip, Boolean isittree){
		int count = 0;
		if(isittree){
		writefile.println("Most popular trees:\r\n");
		}else
		{
		writefile.println("Most green ZIP codes:\r\n");	
		}
		for(int i = treeOrzip.size() - 1; i > 0; i --){
			if( Counting(treeOrzip.get(i),isittree) != 0 && count != 3 && Counting(treeOrzip.get(i),isittree) > Counting(treeOrzip.get(i-1),isittree)&& Counting(treeOrzip.get(i),isittree) != Counting(treeOrzip.get(i-1),isittree)){				
				writefile.printf("%s %d\r\n", translate(treeOrzip.get(i),isittree),Counting(treeOrzip.get(i),isittree));
				count++;
			} else if (Counting(treeOrzip.get(i),isittree) != 0 && count != 3){
				writefile.printf("%s %d\r\n", translate(treeOrzip.get(i),isittree),Counting(treeOrzip.get(i),isittree));
					}	
			}		
	}
	/**
	 * It loops from the beginning of an Sorted Arraylist which should have the least
	 * count when passed into the Counting function. This will then place the string
	 * into a buffer arraylist. The buffer arraylist will write to the file from the last
	 * index to the first for correct formatting. This will also ignore strings with 0 
	 * frequency.
	 * @param treeOrzip treeOrzip either the ArrangedZipcode or NoRepeatTreeCodes will be passed in
	 * @param isittree tells the function whether it should be working according to
	 * whether it should be find the most popular tree or the most green zip codes.
	 */
	public void getLeastFreq(ArrayList<String> treeOrzip, Boolean isittree){
		int count = 0;
		ArrayList<String> Buffer = new ArrayList<String>();
		if(isittree){
		writefile.println("Least popular trees:\r\n");
		}else{
		writefile.println("Least green ZIP codes:\r\n");	
		}
			
		for(int i = 0; i < treeOrzip.size() - 1; i ++){
		
			if( Counting(treeOrzip.get(i),isittree) != 0 && count != 3 && Counting(treeOrzip.get(i),isittree) < Counting(treeOrzip.get(i+1),isittree)&& Counting(treeOrzip.get(i),isittree) != Counting(treeOrzip.get(i+1),isittree)){
				Buffer.add(treeOrzip.get(i));
				
				count++;
			} else if (Counting(treeOrzip.get(i),isittree) != 0 && count != 3 ){
				Buffer.add(treeOrzip.get(i));
				
					}	
			}
		for(int i = Buffer.size() - 1; i >= 0; i --){
			writefile.printf("%s %d\r\n", translate(Buffer.get(i),isittree),Counting(Buffer.get(i),isittree));
		}
	}



}
